---
title: Projeto
permalink: 
lang: pt-BR
prev: "/"
next: "/executivo/"

---
# Projeto

## Componentes

### Escavação

As paredes devem estar livres de pedras e raizes. O fundo deve ser compactado. Pode-se usar mistura de areia e cimento como base.

### Mantas

Mantas servem para impermeabilizar o solo para manter a água no lago. Existem diversos maneiras de impermeabilização, mas as mantas oferecem a vantagem de serem maleáveis e resistêntes. Para o lago sustentar vida biológica, as mantas precisam ser inertes e não alterar a composição da água. Mantas que atualmente cumprem estes requisitos são as mantas de PVC e EPDM. Custo por metro quadrado: **R$ 20**

Para proteger a manta impermeável mecânicamente contra raízes, pedras e qualquer outro dano, coloca-se manta geotêxtil tanto por fora, quanto por dentro.

### Bombas

Bombas fazem com que a água seja forçada pelos filtros e seja oxigenada.

A [recomendação](https://www.youtube.com/watch?v=vgTtzKZQC4Y) encontrada é de 2x o volume do lago por hora.
Se o lago possui 1000 l, usar uma bomba 2000 l/h

Outra [recomedação de uma fabricante de bombas](https://blog.cubos.com.br/bomba-para-lagos/?utm_source=Cubos+Lagos&utm_campaign=c3b899614c-EMAIL_CAMPAIGN_2017_12_03_COPY_06&utm_medium=email&utm_term=0_6767416f79-c3b899614c-252875001&mc_cid=c3b899614c&mc_eid=fe57721f29) é de 5x o volume. 

### Filtros

Eliminam compostos para que a água seja cristalina e saudável aos peixes e plantas.


#### Carvao ativado

#### Filtro biológico

#### Filtro Ultravioleta

Caso o lago pegue sol, precisa de filtro UV.
Na [recomendação](https://www.youtube.com/watch?v=vgTtzKZQC4Y)  é usar 18w para 2 mil litros.

### Decorações

#### Plantas

#### Rochas e pedras

Função decorativa mas também auxiliam na manutenção da temperatura da água.
Atentar-se para pH que a rocha deixará a água. Carpas preferem pH 7,5. Deixar pedra em água por 2 dias e verificar.
Preferir seixos rolados de rio grandes pois são lisas e assim evitam danificiar a manta e os peixes quando se escondem entre elas. Quanto maiores melhor.

O metro cúbico de seixos rolados de rio a partir de **R$ 40** o m3.

#### Areia

A areia deve ser branca e lavada, sem material orgânico.

#### Peixes

### Manutenções

## Variáveis de projeto

### Localização

Deve-se levar em conta o posionamento do sol, visibilidade do lago, naturalidade, nível do terreno entre outros...

#### Sol

A presença de luz solar no lago faz com que algas se proliferem de maneira rápida, exigindo assim a instação de filtro UV.

#### Chuva

Em casos de chuvas fortes, onde será a extravazão do lago? A água da chuva que cai no terreno não pode entrar em contato

### Profundidade do lago

No mínimo 60 cm. Quanto maior o nível, mais impressão de transparência pode ser obtida, desde que a filtragem seja eficiente. Quanto maior, mais estável é a temperatura da água. Lagos podem ser construídos na altura do terreno ou elevados (semi-enterrados), mas assim quebram a naturalidade.

Grandes profundidades podem gerar paredes de grande angulação, o que dificulda a instalação de pedras nas paredes.

### Plantas

Não podem soltar grande quantidade de folhas no lago.
Não podem ter raízes grandes e fortes.

### Fonte de água e energia

## Esquema hidráulico

Fonte de água -(1)-> Lago -(2)-> Bomba -(3)-> Filtro -(4)-> Lago

1 - Torneira com registro, tubulação ou mangueira
2 - Filtro para impedir materiais grossos
3 - Encanamento, flanges, cotovelos, registro com união rosqueável, encanamento, flange
