---
title: Projeto executivo e custos
permalink: 
lang: pt-BR
prev: /projeto/
next: /manutenção/
---

# Projeto executivo


## Escavação

### Antes do início

Atentar-se para a existẽncia de fios de energia, canos d'água e esgoto.

Demarcar com mangueira ou cal.

Prever descarte do solo. Lembrando que cada metro cúbido de solo pode pesar até 1.8 t. 

Verificar previsão de chuvas.

### Compactação

Compactar o fundo da escavação e retirar pedras e raízes.

### Camada de areia

Pode ser usada uma mistura de 1 parte de cimento para 10 de areia para melhorar a camada. O cimento reagirá com a umidade do solo formando uma camada protetora contra raízes e pequenas pedras. 

### Nível

Em casos de chuvas fortes, a água do terreno não pode escorrer para o lago.

Também verificar por onde será a vazão da água no caso de extravazamento. 

## Rochas

Esfregar pedras (preferncialmente usar lavador de pressão) e verificar que não liberam partículas que alteram o pH da água.
Na primeira fileira de pedras tentar usar pedras maiores e afastar um pouco da borda, para criar um ângulo melhor para a próxima fiada.
Nas fiadas seguintes tentar deixar a borda superior o mais reto possível para que ao final na interface tenha um acabamento melhor.

Ao final, esconder bem a manta geotêxtil e evitar que ela encoste no solo para não ocorrer fuga da água por higroscopia.

Deixar para recortar as mantas após todo o processo.

## Bomba

Não deixar a bomba em contato com a areia

## Filtro

Instalar flange com a borracha para dentro.

# Materiais e custos

## Componentes filtro

- Bombona
- Esponja 
- Perlon 10 metros
- Carvão biológico (1g/litro de agua)
- Midias biologicas de quartzo de vidro
- Filtro UV

## Materiais hidráulicos

- 3x Registro globo (abastecimento -> lago -> filtro -> filtro uv)
- Tubulação de 25mm, 40mm

### Equipamentos

- Caçamba
- Carrinho de mão
- Pás
- Tesoura
- 
## Checklist

### Manta geotêxtil

Não deixar a manta geotêxtil interior encostar no lado de para fora. Para que ela não infiltre no tecido e vá para fora do lago. Isto irá abaixar o nível da água. 



## Referências

[Lago Rafael Rohden](https://www.youtube.com/watch?v=fKmpGggA64k)