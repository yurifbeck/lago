---
title: Sobre
permalink: 
lang: pt-BR
prev: /manutenção/
---

# Sobre

## Este site

Esta página é uma tentativa de mostrar interativamente todos as características e passos tomados durante a concepção do projeto.

## Sobre o autor

[Yuri Becker](https://yuribecker.com.br)

[CV](https://cv.yuribecker.com.br)