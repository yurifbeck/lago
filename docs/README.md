---
home: true
heroText: Lago ornamental
heroImage: hero.jpeg
tagline: Projeto completo de lago ornamental
actionText: Projeto →
actionLink: "./projeto/"
features:
- title: Projeto
  details: Concepção, design, custos e análise das variáveis de projeto
- title: Execução
  details: Passa-a-passo, cuidados e metodologias
- title: Manutenção
  details: Cronogramas preventivos, rações, alertas e mais
footer: Produzido por FuruBeck Engenharia

---
