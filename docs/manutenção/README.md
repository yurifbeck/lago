---
title: Manutenção
permalink: 
lang: pt-BR
prev: /executivo/
next: /sobre
---

# Manutenção

## Manta

A manta EPDM tem estimativa de vida de 20 a 30 anos.

## Peixes

Os peixes devem ser adaptados ao clima, à profundidade do lago e aos outros peixes.

### Quantidade

A quantidade de peixes deve respeitar a capacidade do lago e filtros. 

### Ração

Usar rações de qualidade, uma [recomendação](https://www.youtube.com/watch?v=vgTtzKZQC4Y) é a Tropical.

