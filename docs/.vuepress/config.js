module.exports = {
  dest: 'public',
  plugins: [
    ['@vuepress/plugin-back-to-top'], 
    ['@vuepress/active-header-links', 
      {
        sidebarLinkSelector: '.sidebar-link',
        headerAnchorSelector: '.header-anchor',
        headerTopOffset: 120
      }
    ],
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-89398082-3' // UA-00000000-0
      }
    ],
    [
      'vuepress-plugin-medium-zoom'
    ],
    ['@vuepress/nprogress'],
    ['vuepress-plugin-smooth-scroll'],
    ['vuepress-plugin-mathjax']
  ],
  title: 'Lago Ornamental',
  description: 'Apresentação e Projetos',
  themeConfig: {
    logo: '/assets/img/Icon3.png',
    // repo: 'https://gitlab.com/yurifbeck/lago',
    // docsDir: 'docs',
    // Customising the header label
    // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
    // repoLabel: 'Git',

    // if your docs are in a specific branch (defaults to 'master'):
    docsBranch: 'master',
    // defaults to false, set to true to enable
    editLinks: true,
    // custom text for edit link. Defaults to "Edit this page"
    editLinkText: 'Editar página no GitLab!',
    nav: [
          { text: 'Projeto', link: '/projeto/' },
          { text: 'Execução e Custos', link: '/executivo/'},
          { text: 'Manutenção', link: '/manutenção/'},
          {
            text: 'Sobre',
            link: '/sobre/'
          },
          { text: 'Editar', link: '/admin/' }
    ],
    sidebar: {
      '/projeto/': [
        '/projeto/',
      ],
      '/executivo/': [
        '/executivo/',
      
      ],
      '/manutenção/' : [
        '/manutenção/',
      ],
      '/sobre' : [
        '/sobre'
      ],
      '/' : [
        '/',
        '/projeto/',
        '/executivo/',
        '/manutenção/',
        '/sobre'
      ]
    },
    // sidebar: 'auto',
    sidebarDepth: 2,
    smoothScroll: true,
    lastUpdated: 'Última modificação',
    searchPlaceholder: 'Pesquisar...'
  }
}
